/*!
 * Copyright © 2019 Orderful Inc.
 */
export declare type StepType = 'update' | 'transform';
export declare type RecordName = 'purchaseorder' | 'salesorder' | 'inventorydetail' | 'inventoryassignment' | 'item' | 'inventorynumber';
export declare type AssignmentType = 'const' | 'lookup' | 'date';
export interface Assignment {
    type: AssignmentType;
    value: LookupAssignment | string;
}
export interface LookupAssignment {
    recordName: string;
    searchParams: {
        [key: string]: string;
    };
    returnField: string;
}
export interface FieldUpdate {
    [key: string]: Assignment;
}
export interface FieldInsert {
    [key: string]: Assignment;
}
export interface RecordIdentifier {
    type: AssignmentType;
    key: string;
    value: LookupAssignment | string;
}
export interface UpdateStepDetails {
    name: RecordName;
    recordIdentifier: RecordIdentifier;
    updates?: FieldUpdate[];
    inserts?: FieldInsert[];
    subrecords?: Subrecord[];
    sublists?: Sublist[];
}
export interface TransformStepDetails extends UpdateStepDetails {
    target: string;
}
export interface Subrecord {
    recordName: RecordName;
    updates?: FieldUpdate[];
    inserts?: FieldInsert[];
    subrecords?: Subrecord[];
    sublists?: Sublist[];
}
export interface ListItemIdentifier {
    type: AssignmentType;
    key: string;
    value: LookupAssignment | string;
}
export interface Sublist {
    listName?: string;
    listIdentifier?: ListItemIdentifier;
    updates?: FieldUpdate[];
    inserts?: FieldInsert[];
    subrecords?: Subrecord[];
    sublists?: Sublist[];
}
export interface Step {
    stepType: StepType;
    stepDetails: TransformStepDetails | UpdateStepDetails;
}
export interface InboundRequest {
    requestId: string;
    steps: Step[];
}
