export interface EventResponse {
    data: Data;
    meta: Meta;
}
export interface Data {
    record: Record;
    items: Items[];
    itemCount: string;
    itemWeight: string;
    billingAddress: Address;
    shippingAddress: Address;
}
export interface Record {
    id: string;
    type: string;
    fields: {
        test: string;
    };
}
export interface Items {
    internalid: string;
    quantity: string;
    units: string;
    custcol_item_upc: string;
    description: string;
    itemid: string;
    upccasecode: string;
    linenumber: string;
}
export interface Address {
    fields: AddressFields;
}
export interface AddressFields {
    country: string;
    state: string;
    zip: string;
    addr1: string;
    addressee: string;
    addrtext: string;
}
export interface Meta {
    organizationId: string;
    transactionType: string;
    stream: 'test' | 'live';
    from: ToFrom;
    to: ToFrom;
    type: Type;
}
export interface ToFrom {
    idType: 'isaId';
    id: string;
}
export interface Type {
    name: string;
}
