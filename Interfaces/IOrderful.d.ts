import { EventResponse } from '../Models/Request';
import * as https from 'N/https';
import { Query } from '../Objects/Query';
import { Pagination } from '../Objects/Pagination';
import { Data } from '../Objects/Data';
import { BaseOptions } from './Base';
export interface sendDataBlobOptions extends BaseOptions {
    dataBlob: EventResponse;
    OK: () => void;
}
export interface sendStatusOptions extends BaseOptions {
    requestId: string;
    statusCode: 'ERROR' | 'SUCCESS';
    message: string;
    OK: () => void;
}
export interface GetTransactionOptions extends BaseOptions {
    offset?: number;
    limit?: number;
    ownerId?: string;
    direction?: 'in' | 'out';
    transactionType?: 'ORDER' | 'ORDER_ACKNOWLEDGEMENT' | 'INVOICE' | 'PAYMENT' | 'SHIPMENT' | 'RECEIPT_ADVICE' | 'ORDER_STATUS_REPORT' | 'ORDER_CHANGES' | 'CARRIER_LOAD_TENDER' | 'CARRIER_LOAD_TENDER_RESPONSE' | 'CARRIER_STATUS_MESSAGE';
    status?: string;
    partnerId?: number;
    stream?: 'test' | 'live';
    seen?: 'true' | 'false';
    withOrgId?: 'true' | 'false';
    withoutMessage?: 'true' | 'false';
    dateFrom?: string;
    OK: (response: OrderfulResponse) => void;
}
export declare type OrderfulCallBack = (clientResponse: https.ClientResponse) => void;
export interface OrderfulResponse {
    query: Query;
    pagination: Pagination;
    data: Data[];
}
