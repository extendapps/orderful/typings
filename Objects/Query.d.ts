export interface Query {
    seen: boolean;
    revision: string;
    ownerId: string;
    offset: number;
    limit: number;
}
