export interface Data {
    from: {
        idType: string;
        id: string;
    };
    to: {
        idType: string;
        id: string;
    };
    type: string;
    stream: string;
    message: any;
}
