import { Links } from "./Links";
export interface Pagination {
    offset: number;
    limit: number;
    total: number;
    links: Links;
}
