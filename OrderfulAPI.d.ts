import { sendDataBlobOptions, sendStatusOptions } from './Interfaces/IOrderful';
export interface OrderfulAPI {
    getSTREAM(): string;
    getORGANIZATIONID(): string;
    sendDataBlob(options: sendDataBlobOptions): void;
    sendStatus(options: sendStatusOptions): void;
}
